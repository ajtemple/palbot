package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
)

func init() {
	fmt.Println("Initialising environment")
	// get working directory
	cmd, err := exec.Command("pwd").Output()
	if err != nil {
		fmt.Println("Failed to get working directory")
		os.Exit(1)
	}

	os.Setenv("DIR", strings.TrimSuffix(string(cmd), "\n"))
	fmt.Println("Working directory: ", os.Getenv("DIR"))
}

func main() {
	// .env file must contain TOKEN field with the bot token
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Failed to load environment variables")
		return
	}

	Token := os.Getenv("TOKEN")

	discord, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("Error opening connection, ", err)
		return
	}

	discord.AddHandler(handleCommand)
	// specify intent so bot can read/write messages
	discord.Identify.Intents = discordgo.IntentsGuildMessages

	err = discord.Open()
	if err != nil {
		fmt.Println("Error opening connection: ", err)
		return
	}

	fmt.Println("\nBot started, listening for commands\nPress CTRL-C to exit")
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sig

	fmt.Println("\nReceived stop signal, shutting down")
	discord.Close()
}

func handleCommand(session *discordgo.Session, msg *discordgo.MessageCreate) {
	// ignore messages sent by the bot and/or not starting with !pal
	if msg.Author.ID == session.State.User.ID || !strings.HasPrefix(msg.Content, "!pal") {
		return
	}

	// grab bot avatar to use as message image
	user, err := session.User("@me")
	if err != nil {
		fmt.Println("Failed to grab user details")
	}
	avatar := user.AvatarURL("")

	fmt.Println("\nReceived command: ", msg.Content)

	cmd := strings.Fields(msg.Content)[1]

	// !pal help
	if cmd == "help" {
		scripts, err := os.ReadDir(os.Getenv("DIR") + "/scripts/")
		if err != nil {
			fmt.Println("Error reading from scripts directory: ", err)
			return
		}

		var m string
		for _, s := range scripts {
			m += s.Name() + "\n"
		}

		// TODO: move reply messages into function with error handling
		_, err = session.ChannelMessageSendEmbedReply(
			msg.ChannelID,
			&discordgo.MessageEmbed{
				Type:        discordgo.EmbedTypeRich,
				Title:       "Commands",
				Description: "Available commands: ```\n" + m + "```",
				Color:       1752220,
				Image: &discordgo.MessageEmbedImage{
					URL: avatar,
				},
			},
			msg.Reference(),
		)

		if err != nil {
			session.ChannelMessageSend(msg.ChannelID, "Error sending reply, check logs")
			fmt.Println(err)
		}
		return
	}

	// commands other than help are defined by filenames in ./scripts/ folder
	filename := os.Getenv("DIR") + "/scripts/" + cmd

	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		fmt.Println("Unknown command: ", cmd)
		session.ChannelMessageSendEmbedReply(
			msg.ChannelID,
			&discordgo.MessageEmbed{
				Type:        discordgo.EmbedTypeRich,
				Title:       "Error",
				Description: "Unrecognised command: " + cmd + ". Use `!pal help` for a list of commands.",
				Color:       15548997,
				Image: &discordgo.MessageEmbedImage{
					URL: avatar,
				},
			},
			msg.Reference(),
		)
		return
	}

	session.ChannelMessageSendEmbedReply(
		msg.ChannelID,
		&discordgo.MessageEmbed{
			Type:        discordgo.EmbedTypeRich,
			Title:       "PalBot",
			Description: "Running command: " + cmd,
			Color:       1146986,
			Footer: &discordgo.MessageEmbedFooter{
				Text: "Wait for output.",
			},
			Image: &discordgo.MessageEmbedImage{
				URL: avatar,
			},
		},
		msg.Reference(),
	)

	fmt.Println("Running script: ", filename)
	out, err := exec.Command(filename).Output()
	if err != nil {
		fmt.Println("Error running command: ", err)
		return
	}

	// strip ANSI chars - LinuxGSM command outputs contain a lot of terminal escape chars (\x1b[)
	const ansi = "[\u001B\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[a-zA-Z\\d]*)*)?\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PRZcf-ntqry=><~]))"
	reg := regexp.MustCompile(ansi)

	output := reg.ReplaceAllString(string(out), "")
	output = strings.TrimSuffix(output, "\n")

	output_fields := strings.Split(output, "\n")
	output_len := len(output_fields)

	output = output_fields[output_len-1]

	// handle carriage returns in output
	if strings.Contains(output, "\r") {
		f := strings.Split(output, "\r")
		output = f[len(f)-1]
	}

	fmt.Println("Output: ", output)

	session.ChannelMessageSendEmbed(
		msg.ChannelID,
		&discordgo.MessageEmbed{
			Type:        discordgo.EmbedTypeRich,
			Title:       "Output",
			Description: "```" + output + "```",
			Color:       0,
		},
	)
}
